﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Helper.Controls
{
    public class ImageHelper
    {
        public BitmapImage CreateBitmapImage(string imgUrl)
        {
            //方式1，直接通过图片路径
            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.UriSource = new Uri(imgUrl);
            bmp.EndInit();

            //方式2，通过字节数组
            //FileStream fs = new FileStream(imgUrl, FileMode.Open);
            //byte[] buffer = new byte[fs.Length];
            //fs.Read(buffer, 0, buffer.Length);
            //fs.Close();
            //BitmapImage bmp = new BitmapImage();
            //bmp.BeginInit();
            //bmp.StreamSource = new MemoryStream(buffer);
            //bmp.EndInit();

            //https://blog.csdn.net/i1tws/article/details/77334383
            return bmp;
        }

        /// <summary>
        /// 图片沿中心位置旋转
        /// </summary>
        /// <param name="img"></param>
        public void RotateImage_Start(Image img)
        {
            // 图片旋转
            RotateTransform rotateTransform = new RotateTransform();
            img.RenderTransform = rotateTransform;
            img.RenderTransformOrigin = new Point(0.5, 0.5);
            //rotateTransform.Angle = 90; //设置旋转角度为90度

            DoubleAnimation animation = new DoubleAnimation();
            animation.From = 0; //动画起始值为0度
            animation.To = 360; //动画结束值为360度
            animation.Duration = TimeSpan.FromSeconds(2); //动画持续时间为2秒
            animation.RepeatBehavior = RepeatBehavior.Forever; //设置动画重复次数为无限
            rotateTransform.BeginAnimation(RotateTransform.AngleProperty, animation);
        }
        public void RotateImage_Stop(Image img)
        {
            img.RenderTransform = null;
        }
    }
}
