﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace xjrh_prism_wpf.Common
{
    public class PasswordHelper
    {

        static bool _isUpdating = false;

        #region 密码框的处理
        public static readonly DependencyProperty PasswordProperty =
          DependencyProperty.RegisterAttached(
              "Password", typeof(string), typeof(PasswordHelper),
              new FrameworkPropertyMetadata("", new PropertyChangedCallback(OnPropertyChanged))
              );

        public static string GetPassword(DependencyObject d)
        {
            return d.GetValue(PasswordProperty).ToString();
        }
        public static void SetPassword(DependencyObject d, string value)
        {
            d.SetValue(PasswordProperty, value);
        }

        private static void OnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PasswordBox password = d as PasswordBox;
            password.PasswordChanged -= Password_PasswordChanged;
            if (!_isUpdating)
            {
                password.Password = e.NewValue?.ToString();
            }
            password.PasswordChanged += Password_PasswordChanged;
        }
        #endregion



        #region 附加属性的处理
        public static readonly DependencyProperty AttachProperty =
        DependencyProperty.RegisterAttached(
       "Attach", typeof(bool), typeof(PasswordHelper),
       new FrameworkPropertyMetadata(default(bool), new PropertyChangedCallback(OnAttached))

       );

        public static bool GetAttach(DependencyObject d)
        {
            return (bool)d.GetValue(AttachProperty);
        }
        public static void SetAttach(DependencyObject d, bool value)
        {
            d.SetValue(AttachProperty, value);
        }

        private static void OnAttached(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PasswordBox password = d as PasswordBox;
            password.PasswordChanged += Password_PasswordChanged;
        }

        private static void Password_PasswordChanged(object sender, RoutedEventArgs e)
        {
            PasswordBox passwordBox = sender as PasswordBox;
            _isUpdating = true;
            SetPassword(passwordBox, passwordBox.Password);
            _isUpdating = false;
        }
        #endregion


    }
}
