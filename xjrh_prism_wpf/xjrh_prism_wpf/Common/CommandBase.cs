﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace xjrh_prism_wpf.Common
{
    /// <summary>
    /// 界面控件触发，后台相应的接口类
    /// </summary>
    public class CommandBase : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
          return DoCanExecute?.Invoke(parameter)==true;
        }

        public void Execute(object parameter)
        {
            DoEcecute?.Invoke(parameter);
        }

        public Action<object>DoEcecute { get; set; }
        public Func<object,bool> DoCanExecute { get; set; } 
    }
}
