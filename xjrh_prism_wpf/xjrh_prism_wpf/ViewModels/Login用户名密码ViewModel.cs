﻿using Helper.Controls;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using xjrh_prism_wpf.Models;


namespace xjrh_prism_wpf.ViewModels
{
    public class Login用户名密码ViewModel : BindableBase
    {
        // 定义变量
        private BitmapImage logoimg;
        private string errorMsg;
        private Image image_Logo = new Image();

        // 定义属性
        public BitmapImage Logoimg
        {
            get { return logoimg; }
            set { logoimg = value;
                RaisePropertyChanged();
            }
        }
        public string ErrorMsg
        {
            get { return errorMsg; }
            set { errorMsg = value;
                RaisePropertyChanged();
            }
        }
        public Image Image_Logo
        {
            get { return image_Logo; }
            set { image_Logo = value;
                RaisePropertyChanged();
            }
        }
     


        public LoginModel LoginModel { get; set; }

        //定义委托
        public DelegateCommand<Image> RotateImage_Start { get; private set; }
        public DelegateCommand<Image> RotateImage_Stop { get; private set; }
        //构造函数
        public Login用户名密码ViewModel()
        {
            // 1. 加载图片
            ImageHelper imageHelper = new ImageHelper();
            string path = Environment.CurrentDirectory + "\\Images\\Logo.png"; 
            Logoimg = imageHelper.CreateBitmapImage(path);
            Image_Logo = new Image();
            Image_Logo.Source = Logoimg;
            //普通按钮点击命令
            RotateImage_Start = new DelegateCommand<Image>(RotateStart);
            RotateImage_Stop = new DelegateCommand<Image>(RotateStop);
            //初始化
            this.LoginModel = new LoginModel() { UserName = "admin" , Password = "123123"};
        }

        //函数调用

        private void RotateStart(object img)
        {
            ImageHelper imageHelper = new ImageHelper();
            Image ig = img as Image;
            imageHelper.RotateImage_Start(ig);
            ErrorMsg = "点击了图片旋转，开始旋转图片！";
        }

        private void RotateStop(object img)
        {
            ImageHelper imageHelper = new ImageHelper();
            Image ig = img as Image;
            imageHelper.RotateImage_Stop(ig);

            ErrorMsg = "点击了停止旋转，图片旋转停止";
        }

      

    }
}
