﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls.Primitives;
using xjrh_prism_wpf.Models;
using xjrh_prism_wpf.Models.EventMsg;

namespace xjrh_prism_wpf.ViewModels
{
    public class DeviceListViewModel : BindableBase
    {

        // 功能区默认的行高
        private string heigh_Fun = "*";
        public string Heigh_Fun
        {
            get { return heigh_Fun; }
            set
            {
                heigh_Fun = value;
                RaisePropertyChanged();
            }
        }
        private List<Device> devices;

        public List<Device> Devices
        {
            get { return devices; }
            set
            {
                devices = value;
                RaisePropertyChanged();
            }
        }

      
        public DeviceListViewModel()
        {
            Devices = new List<Device>();
            Devices.Add(new Device() {  DeviceName ="设备1",  IPAddress = "192.168.0.1",  CarNumber = "【1】", IsOnLineColor = "Green" });
            Devices.Add(new Device() {  DeviceName ="设备2", IPAddress = "192.168.0.2", CarNumber = "【2】", IsOnLineColor = "Transparent" });
            Devices.Add(new Device() {  DeviceName ="设备3", IPAddress = "192.168.0.3", CarNumber = "【3】", IsOnLineColor = "Transparent" });
            Devices.Add(new Device() {  DeviceName ="设备4", IPAddress = "192.168.0.4", CarNumber = "【4】", IsOnLineColor = "Transparent" });
            Devices.Add(new Device() {  DeviceName ="设备5", IPAddress = "192.168.0.5", CarNumber = "【5】", IsOnLineColor = "Transparent" });
            Devices.Add(new Device() {  DeviceName ="设备6", IPAddress = "192.168.0.6", CarNumber = "【6】", IsOnLineColor = "Transparent" });

           
        }

       
    }
}
