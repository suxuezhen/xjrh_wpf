﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using xjrh_prism_wpf.Common;
using xjrh_prism_wpf.Views;

namespace xjrh_prism_wpf.ViewModels
{
    public class LoginViewModel : BindableBase
    {
        private CommandBase closeWindowCommand; 

        public CommandBase CloseWindowCommand
        { 
            get { return closeWindowCommand; }
            set { closeWindowCommand = value; }
        }

        private string titleName;

        public string TitleName
        {
            get { return titleName; }
            set { titleName = value; }
        }

        private string version;

        public string Version
        { 
            get { return version; }
            set { version = value; }
        }


        private string closeBackColor;

        public string CloseBackColor
        {
            get { return closeBackColor; }
            set { closeBackColor = value;
                RaisePropertyChanged();
            }
        }

      
        public LoginViewModel()
        {
            CloseBackColor = "Transparent"; //默认透明色
            TitleName = "主机测试系统";
            Version = "J4 V2.0-20231023-02";
            this.CloseWindowCommand = new CommandBase();
            this.CloseWindowCommand.DoEcecute = new Action<object>(
                (o) => {               
                    Login login = o as Login;                 
                    login.Close();
                });
            this.CloseWindowCommand.DoCanExecute = new Func<object, bool>(
              (o) => 
              {
                  return true;
              });

        }


    }
}
