﻿using Prism.Mvvm;
using Prism.Regions;
using xjrh_prism_wpf.UserControls;
using xjrh_prism_wpf.Views;

namespace xjrh_prism_wpf.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private string _title = "西交瑞航主机测试系统";
        private readonly IRegionManager _regionManager;

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public MainWindowViewModel(IRegionManager regionManager)
        {
            this._regionManager = regionManager;
            regionManager.RegisterViewWithRegion("MyToolBar", typeof(MyToolBar));
            regionManager.RegisterViewWithRegion("MyContent", typeof(MyContent));
            regionManager.RegisterViewWithRegion("MyStatusBar", typeof(MyStatusBar));
        }
    }
}
