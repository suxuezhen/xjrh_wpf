﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using xjrh_prism_wpf.Models.EventMsg;

namespace xjrh_prism_wpf.ViewModels
{
	public class MyStatusBarViewModel: BindableBase
    {
        // 按钮的图标切换
        private string icon;
        // 按钮的文本切换
        private string btnTxt;

        public string Icon
        {
            get { return icon; }
            set { icon = value;
                RaisePropertyChanged();
            }
        }
        public string BtnTxt
        {
            get { return btnTxt; }
            set
            {
                btnTxt = value;
                RaisePropertyChanged();
            }
        }

        private readonly IEventAggregator eventAggregator;  // 发送消息用的

        public DelegateCommand<string> SendCommand { get; private set; }

        public MyStatusBarViewModel(IEventAggregator eventAggregator)
        {
            Icon = "ChevronDown"; // 隐藏日志 默认
            BtnTxt = "隐藏日志";



            this.eventAggregator = eventAggregator;
            SendCommand = new DelegateCommand<string>(ms =>
            {
                string cmd = "Log_Hide";
                //改变按钮显示
                if (Icon== "ChevronDown")
                {
                    Icon = "ChevronUp";
                    BtnTxt = "显示日志";
                }
                else 
                {
                    cmd = "Log_Show";
                    Icon = "ChevronDown";
                    BtnTxt = "隐藏日志";
                }

                //发布一条消息
                eventAggregator.GetEvent<MessageEvent>().Publish(cmd);
            });
        }
	}
}
