﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Windows.Controls;
using xjrh_prism_wpf.Models.EventMsg;
using xjrh_prism_wpf.UserControls;

namespace xjrh_prism_wpf.ViewModels
{
    public class MyContentViewModel : BindableBase
    {
        // Grid 总高
        private string heigh_All = "*";
        // 功能区默认的行高
        private string heigh_Fun = "3*";
        // 拖拽条 默认的行高
        private string heigh_Spl = "auto";
        // 日志栏 默认的行高
        private string heigh_Log = "*";
        // 日志 是否显示
        private string visibility = "Visible";

        public string Heigh_All
        {
            get { return heigh_All; }
            set
            {
                heigh_All = value; 
                RaisePropertyChanged();
            }
        }

        public string Heigh_Fun
        {
            get { return heigh_Fun; }
            set
            {
                heigh_Fun = value;
                RaisePropertyChanged();
            }
        }
        public string Heigh_Spl
        {
            get { return heigh_Spl; }
            set
            {
                heigh_Spl = value;
                RaisePropertyChanged();
            }
        }
        public string Heigh_Log
        {
            get { return heigh_Log; }
            set { heigh_Log = value;
                RaisePropertyChanged();
            }
        }
        public string Visibility 
        {
            get { return visibility; }
            set
            {
                visibility = value;
                RaisePropertyChanged();
            }
        }


        private string windowHeigh = "auto";

        public string WindowHeigh
        {
            get { return windowHeigh; } 
            set { windowHeigh = value;
                RaisePropertyChanged();
            }
        }


        private readonly IRegionManager _regionManager;
        private readonly IEventAggregator eventAggregator;  // 发送和接收消息用的

        public MyContentViewModel(IRegionManager regionManager, IEventAggregator eventAggregator)
        {
            // 区域
            this._regionManager = regionManager;
            regionManager.RegisterViewWithRegion("DeviceList", typeof(DeviceList));

            //消息
            this.eventAggregator = eventAggregator;                

            eventAggregator.GetEvent<MessageEvent>().Subscribe(OnMessageReceived, ThreadOption.PublisherThread, false, msg =>
            {
                if (msg.IndexOf("Log") != -1)
                {
                    return true;    // 复合调节的执行
                }
                else
                {
                    return false; //不符合条件的过滤掉
                }
            });
        
        }



        private void OnMessageReceived(string obj)
        {
            string a = WindowHeigh;
            string b = a.ToString();

            if (obj == "Log_Show")
            {
                Heigh_Fun = "3*";
                Heigh_Spl = "auto";
                Heigh_Log = "*";
                Visibility = "Visible";
            }
            else
            {
                Heigh_Fun = "*";
                Heigh_Spl = "0";
                Heigh_Log = "0";
                Visibility = "Collapsed";
            }
           
        }

       
   　 


    }
}
