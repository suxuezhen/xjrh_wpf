﻿using Helper.Controls;
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;

namespace xjrh_prism_wpf.Views
{
    /// <summary>
    /// Interaction logic for Login用户名密码.xaml
    /// </summary>
    public partial class Login用户名密码 : Window
    {
        public Login用户名密码()
        {
            InitializeComponent();

      
        }

        private void Grid_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ChangedButton == System.Windows.Input.MouseButton.Left)
            {
                this.DragMove();
            }
        }

      

        //停止旋转
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            logoImage.RenderTransform = null;
        }

        //开始旋转
        private void start_click(object sender, RoutedEventArgs e)
        {
            // 图片旋转
            RotateTransform rotateTransform = new RotateTransform();
            logoImage.RenderTransform = rotateTransform;
            logoImage.RenderTransformOrigin = new Point(0.5, 0.5);
            //rotateTransform.Angle = 90; //设置旋转角度为90度

            DoubleAnimation animation = new DoubleAnimation();
            animation.From = 0; //动画起始值为0度
            animation.To = 360; //动画结束值为360度
            animation.Duration = TimeSpan.FromSeconds(2); //动画持续时间为2秒
            animation.RepeatBehavior = RepeatBehavior.Forever; //设置动画重复次数为无限
            rotateTransform.BeginAnimation(RotateTransform.AngleProperty, animation);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // 加载时，旋转效果
            ImageHelper imageHelper = new ImageHelper();
            imageHelper.RotateImage_Start(this.logoImage);
        }
    }
}
