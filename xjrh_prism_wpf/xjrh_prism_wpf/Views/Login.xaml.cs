﻿using System.Drawing;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace xjrh_prism_wpf.Views
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Grid_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                this.DragMove();
            }
        }

        private void btn_Close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void closeButtonEnter(object sender, MouseEventArgs e)
        {
            BrushConverter brushConverter = new BrushConverter();
            Brush brush = (Brush)brushConverter.ConvertFromString("Red");        
            //this.closeBtn.Background = brush;
        }

        private void closeButtonLeave(object sender, MouseEventArgs e)
        {
            BrushConverter brushConverter = new BrushConverter();
            Brush brush = (Brush)brushConverter.ConvertFromString("Transparent");
            //this.closeBtn.Background = brush;
        }

        private void btn_ShowMain(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.ShowInTaskbar = true;
            mainWindow.Show();
            this.Hide();
        }
    }
}
