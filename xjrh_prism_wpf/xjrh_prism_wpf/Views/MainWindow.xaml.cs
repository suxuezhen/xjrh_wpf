﻿using System;
using System.Windows;

namespace xjrh_prism_wpf.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void WindowCloseing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //强制退出，即使有其他的线程没有结束
            Environment.Exit(0);
            //关闭当前程序，如果有其他线程没有结束，不会关闭
            //System.Windows.Application.Current.Shutdown();

        }
    }
}
