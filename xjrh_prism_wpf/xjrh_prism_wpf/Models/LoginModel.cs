﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace xjrh_prism_wpf.Models
{
    public class LoginModel : BindableBase
    {
        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set { _userName = value;
                RaisePropertyChanged();
            }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value; 
                RaisePropertyChanged();
            }
        }

        public LoginModel()
        {

        }
    }
}
