﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace xjrh_prism_wpf.Models
{
    public class Device : BindableBase
    {
        private string  deviceName;
        private string ipAddress;
        private string carNumber;
        private string isOnLineColor = "Transparent";         //在线判定 Green 为在线，Transparent 为不在，改变背景色


        public string DeviceName
        { 
            get { return deviceName; }
            set { deviceName = value; 
            RaisePropertyChanged();
            }
        }
        public string IPAddress
        {
            get { return ipAddress; }
            set
            {
                ipAddress = value;
                RaisePropertyChanged();
            }
        }
        public string CarNumber
        {
            get { return carNumber; }
            set
            {
                carNumber = value;
                RaisePropertyChanged();
            }
        }
        public string IsOnLineColor
        {
            get { return isOnLineColor; }
            set {
                isOnLineColor = value;
                RaisePropertyChanged();
            }
        }



        public Device()
        {

        }
    }
}
