﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace xjrh_prism_wpf.Models.EventMsg
{
	public class MessageEvent :  PubSubEvent<string>
	{

        /// 1. Log_Show 显示日志,Show必须匹配
        /// 1. Log_Hide  隐藏日志,Hide非必须匹配，不满足Show即为隐藏
        /// 
        public MessageEvent()
        {

        }
	}
}
