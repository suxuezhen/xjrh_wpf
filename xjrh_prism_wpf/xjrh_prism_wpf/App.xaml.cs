﻿using Prism.Ioc;
using System.Windows;
using xjrh_prism_wpf.UserControls;
using xjrh_prism_wpf.ViewModels;
using xjrh_prism_wpf.Views;

namespace xjrh_prism_wpf
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<Login用户名密码>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<DeviceList, DeviceListViewModel>();
            containerRegistry.RegisterForNavigation<MyContent, MyContentViewModel>();
            containerRegistry.RegisterForNavigation<MyContent, MyContentViewModel>();
            containerRegistry.RegisterForNavigation<MyStatusBar, MyStatusBarViewModel>();
        }
    }
}
